/* *************************************************************************
 * Citygram > CitygramStreamer
 * This is the SuperCollider Class file for Citygram Streaming
 *
 * The Citygram project can be found at http://citygram.smusic.nyu.edu
 * Tae-Hong Park is the Principal Investigator
 *
 * This SuperCollider Library was written by the Citygram team and led
 * by Michael Musick.  Questions can be directed to him at musick@nyu.edu .
 *
 * Disclaimer: We are still in the earlier stages of this project, and
 * 				as such, things are likely to break.  Sorry. We will
 * 				work as quick as is possible to fix any problems that arise.
 *
 * Version 1.0 - 04.23.2015
 **************************************************************************/

CitygramStreamer {

	var <username, <password, <>verbose = 0,
		<uID, <>lat, <>lng, <>z_height = 0;
	var <>featurePollDelay = 0.020, <>pushDelay = 8.0;
	var <rms, <centroid, <flatness, <crest, <flux, <power, <slope, <zeroXing, <mfcc;
	var rmsBus, centBus, flatBus, crestBus, fluxBus, powerBus, slopeBus, xingBus, mfccBus;
	var <totalReadings, startTime, <lastTimestamp;
	var cgSendTask, extractionSynth;

	// CREATE A NEW STREAMER OBJECT
	*new {
		arg username, password, lat, lng, z_height, verbose = 0, uID, updateOnServer = true, forceNewRSD = false;
		var instance;
		// a username and password are required to stream!
		if( (username==nil).or(password==nil), {
			"Please supply a username and password to instantiate class.".postln;
			^ "Not Instantiated.";
		});
		// if verbose is not set then instantiate it to 0
		if( verbose==nil,
			{instance = this.init( username, password, 0, uID ); },
			{instance = this.init( username, password, verbose, uID );}
		);
		// set the class variables for lat, lng, and z_height
		instance.lat=lat; instance.lng=lng; instance.z_height=z_height;
		// call a function to retrieve the uID for this machine and username combination
		if(updateOnServer,{instance.updateUID(forceNewRSD);});
		// load dependent SynthDefs
		this.loadDependentFunction;
		// load dependent Task Functions
		instance.loadDependents;
		// if the call to retrieve the uID was successful, then return this object
		if( (instance.uID).notNil, {^instance}, {^nil});
	}
	// initialize the class with the interpreter
	*init {
		arg username, password, verbose, uID;
		^super.newCopyArgs(username, password, verbose, uID);
	}


	/***************************************************************************************/
	// The following are used to register this computer with the Citygram Server
	/***************************************************************************************/

	// if an RSD is returned that has no lat/lng then try and supply one
	// this is accomplished by finding relative to the IP
	// not usually very accurate.  But within a mile or so.
	findLatLng {
		arg replaceCurrentLatLng = true, verbose;
		var line_, pipe_, lat, lng, verbose_;
		if( verbose!=nil, {verbose_=verbose;}, {verbose_= this.verbose.asInteger});
		line_ = Array.newClear(200);
		pipe_ = Pipe.new(
			"curl http://citygram.calarts.edu/~jturner/getloc.php |
			grep geoplugin_l", "r");
		for( 0, 1, {arg i; line_.put(i, pipe_.getLine);});
		pipe_.close;
		lat = (line_[0].split($'))[3].asFloat;
		lng = (line_[1].split($'))[3].asFloat;
		if( verbose_ > 0, {
			"Request returned lat/lng of ".post; lat.post; "/".post; lng.postln;
		});
		if( replaceCurrentLatLng, {this.lat=lat; this.lng=lng });
		^[lat, lng]
	}

	// GET UID FOR THIS USER AND COMPUTER
	// or if uID is already set, update info on the server
	updateUID {
		arg forceNew = false;
		var pipe_, line_, id, adr, pp, qry, verbose, os;
		verbose = this.verbose;

		if( verbose > 0, {
			"".postln;"".postln;"".postln;
			"CITYGRAM STREAMER".postln;
			"via SuperCollider".postln;
			"Getting Unique Devide ID from Server".postln;
			"".postln;
		});

		// check what kind of system this is
		pipe_ = Pipe.new("uname", "r");
		pp = pipe_.getLine(); pipe_.close;
		if( pp == "Darwin", {os = 0;});
		if( pp == "Linux", {os = 1;});

		// Get Device Information Based on OS Type
		// Darwin (Mac OS X)
		if( os == 0, {
			// get the device ID.
			pipe_ = Pipe.new("ipconfig getifaddr en1", "r");
			pp = pipe_.getLine(); pipe_.close;
			if( pp == nil, {
				pipe_ = Pipe.new("ipconfig getifaddr en0", "r");
				pp = pipe_.getLine(); pipe_.close;
			});
			// get the mac IP
			pipe_ = Pipe.new("ifconfig en0 | grep ether", "r");
			adr = pipe_.getLine(); adr = (adr.split($ ))[1]; pipe_.close;
			// get the IP addr
			pipe_ = Pipe.new("ioreg -l | grep IOPlatformSerialNumber", "r");
			id = pipe_.getLine(); id = (id.split($"))[3]; pipe_.close;
		});
		// Linux
		if( os == 1, {
			// Get the IP
			pipe_ = Pipe.new("hostname -I", "r");
			pp = pipe_.getLine(); pipe_.close;
			// get the MacAddr
			pipe_ = Pipe.new(
				"ifconfig wlan0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'",
				"r");
			adr = pipe_.getLine(); pipe_.close;
			// get the UUID
			pipe_ = Pipe.new("hostid", "r");
			id = pipe_.getLine(); pipe_.close;
		});

		// Create the cURL query used to register the device
		// **check for nil replace with null
		// nil values indicate that the user does not want to update them
		// In the case of lat/lng, the values may not need to be updated
		qry="curl -X POST -d \"user="++this.username++"&pwd="++this.password;
		// if uID was supplied
		if(this.uID!=nil,{qry=qry++"&uid="++this.uID;});
		qry=qry++"&devid="++id++"&mac="++adr++"&ip="++pp;
		if(((this.lat.isNil).or(this.lng.isNil)), {}, {qry=qry++"&lat="++this.lat++"&long="++this.lng;});
		if((this.z_height.isNil), {}, {qry=qry++"&z="++this.z_height;});
		if((forceNew),{qry=qry++"&forceNew=true";});
		qry=qry++"\" ";
		qry=qry++"http://citygram.smusic.nyu.edu/register/register.php";
		if( this.verbose>=1, {
			("    User: "++this.username).postln;
			"      ip: ".post; pp.postln;
			" macAddr: ".post; adr.postln; "".postln;
		});
		if(verbose>2,{qry.postln;});
		// send the info to the world through a pipe to the unix shell
		pipe_ = Pipe.new(qry, "r");
		line_ = pipe_.getLine;
		// close the connection and turn the return into a dictionary object
		pipe_.close; line_ = line_.parseYAML;
		// set whether successful
		if( line_.["auth"]=="true",
			{
				// set this objects uID value accordingly
				uID = line_.["uid"];
				this.lat = line_.["lat"].asFloat;
				this.lng = line_.["lng"].asFloat;
				// check that lat/lng were set on the server
				// or by the user during initialization
				// if they were not attempt to set them
				if( (this.lat == 0).or(this.lng == 0), {
					this.findLatLng(verbose: 0);
					"Warning, lat/lng not supplied".postln;
					"attempting to get lat/lng, please check for accuracy.".postln;
					"     lat: ".post; this.lat.postln;
					"     lng: ".post; this.lng.postln;
					// after getting the lat/lng update them on the server
					qry="curl -X POST -d \"user="++this.username++"&pwd="++this.password;
					qry=qry++"&devid="++id++"&mac="++adr++"&ip="++pp;
					qry=qry++"&lat="++this.lat++"&long="++this.lng++"\" ";
					qry=qry++"http://citygram.smusic.nyu.edu/register/register.php";
					pipe_ = Pipe.new(qry, "r");
					line_ = pipe_.getLine;
					pipe_.close; line_ = line_.parseYAML;
					// set if successful
					if( line_.["auth"]=="true", {
						uID = line_.["uid"];
						this.lat = line_.["lat"].asFloat; this.lng = line_.["lng"].asFloat;
					});
				});
				if( line_.["lastTS"].isFloat, {	lastTimestamp = line_.["lastTS"].asFloat; });
				if( verbose > 0, {"Unique Device ID Returned (uID): ".post; uID.postln;});
			},
			{"REGISTRATION OF THE DEVICE FAILED.".postln;line_.["msg"].postln;}
		);
		// set instance uID
		^uID;
	}

	/***************************************************************************************/
	// The following load all dependent functions of SynthDefs
	// for feature extraction and streaming
	/***************************************************************************************/

	// SynthDefs for feature extraction
	*loadDependentFunction {
		// convenience method for microphone
		SynthDef(\citygramExtractor_SoundIn, {
			arg bus = 0, rmsOut = 0, centOut = 1, flatOut=2, crestOut=3,
				fluxOut=4, powerOut=5, slopeOut=6, xingOut=7, mfccOut=8;
			var input, fftSig;
			var rms, centroid, flatness, crest, flux, power, slope, zeroXing;
			var mfccArr;
			input = SoundIn.ar(bus);
			// time domain feature extraction
			rms = RunningSum.rms( input, 1024 );
			zeroXing = (RunningSum.ar( ZeroCrossing.ar( input ), 1024) / 1024);
			// frequency domain feature extraction
			fftSig = FFT( LocalBuf( 1024 ), input, wintype: 1 );
			centroid = SpecCentroid.kr( fftSig );
			flatness = SpecFlatness.kr( fftSig );
			crest = FFTCrest.kr( fftSig, freqlo: 40, freqhi: 8000);
			flux = FFTFlux.kr( fftSig, normalise: 1 );
			power = FFTPower.kr( fftSig );
			slope = FFTSlope.kr( fftSig );
			mfccArr = MFCC.kr( fftSig, 13 );

			Out.kr(rmsOut, rms);
			Out.kr(centOut, centroid);
			Out.kr(flatOut, flatness);
			Out.kr(crestOut, crest);
			Out.kr(fluxOut, flux);
			Out.kr(powerOut, power);
			Out.kr(slopeOut, slope);
			Out.kr(xingOut, zeroXing);
			Out.kr(mfccOut, mfccArr);
		}).add;
		// for use with an audio bus
		// this can be particularily useful when wanting to stream prerecorded soundscapes
		SynthDef(\citygramExtractor_Bus, {
			arg bus = 0, rmsOut = 0, centOut = 1, flatOut=2, crestOut=3,
				fluxOut=4, powerOut=5, slopeOut=6, xingOut=7, mfccOut=8;
			var input, fftSig;
			var rms, centroid, flatness, crest, flux, power, slope, zeroXing;
			var mfccArr;
			input = In.ar(bus);
			// time domain feature extraction
			rms = RunningSum.rms( input, 1024 );
			zeroXing = (RunningSum.ar( ZeroCrossing.ar( input ), 1024) / 1024);
			// frequency domain feature extraction
			fftSig = FFT( LocalBuf( 1024 ), input, wintype: 1 );
			centroid = SpecCentroid.kr( fftSig );
			flatness = SpecFlatness.kr( fftSig );
			crest = FFTCrest.kr( fftSig, freqlo: 40, freqhi: 8000);
			flux = FFTFlux.kr( fftSig, normalise: 1 );
			power = FFTPower.kr( fftSig );
			slope = FFTSlope.kr( fftSig );
			mfccArr = MFCC.kr( fftSig, 13 );
			Out.kr(rmsOut, rms);
			Out.kr(centOut, centroid);
			Out.kr(flatOut, flatness);
			Out.kr(crestOut, crest);
			Out.kr(fluxOut, flux);
			Out.kr(powerOut, power);
			Out.kr(slopeOut, slope);
			Out.kr(xingOut, zeroXing);
			Out.kr(mfccOut, mfccArr);
		}).add;
	}

	// load or allocate everything else
	loadDependents {
		// get the control buses
		rmsBus = Bus.control(Server.default, 1); rmsBus.set(0.0);
		centBus = Bus.control(Server.default, 1); centBus.set(0.0);
		flatBus = Bus.control(Server.default, 1); flatBus.set(0.0);
		crestBus = Bus.control(Server.default, 1); crestBus.set(0.0);
		fluxBus = Bus.control(Server.default, 1); fluxBus.set(0.0);
		powerBus = Bus.control(Server.default, 1); powerBus.set(0.0);
		slopeBus = Bus.control(Server.default, 1); slopeBus.set(0.0);
		xingBus = Bus.control(Server.default, 1); xingBus.set(0.0);
		mfccBus = Bus.control(Server.default, 13); mfccBus.set(0.0);

		// a task for uploading to the Citygram Server
		cgSendTask = Task({
			var dataArray, qry, verboseBench, verbose, uID, pipe_, line_;
			var numOfReadings=0, numOfFeatures, timeStamp, featurePollDelay_;
			var timeOffset=0;
			// set internal variables
			verbose = this.verbose;
			featurePollDelay_ = featurePollDelay;
			numOfFeatures = (pushDelay / featurePollDelay_).round;
			if(verbose>1,{verboseBench=true},{verboseBench=false});
			// track the total number of readings
			totalReadings=0;
			// set offset time
			if( startTime.notNil,
				{timeOffset=(Date.getDate.rawSeconds - startTime);},
				{timeOffset=0;}
			);
			// Prime the pipes
			rmsBus.get({ arg val; rms = val; });
			centBus.get({ arg val; centroid = val; });
			flatBus.get({ arg val; flatness = val; });
			crestBus.get({ arg val; crest = val; });
			fluxBus.get({ arg val; flux = val; });
			powerBus.get({ arg val; power = val; });
			slopeBus.get({ arg val; slope = val; });
			xingBus.get({ arg val; zeroXing = val; });
			mfccBus.getn(13, { arg val; mfcc = val; });
			// add clean up in case of cmd period stop
			CmdPeriod.doOnce( { this.closeStream; }; );
			1.0.wait;
			// repeat send task till told otherwise
			inf.do({
				// update verbose
				verbose=this.verbose; if(verbose>1,{verboseBench=true},{verboseBench=false});
				// update the send rates
				featurePollDelay_ = featurePollDelay;
				numOfFeatures = (pushDelay / featurePollDelay_).round;
				// get the current timeStamp
				timeStamp = Date.getDate;
				//Get the value from the buses
				rmsBus.get({ arg val; rms = val; });
				centBus.get({ arg val; centroid = val; });
				flatBus.get({ arg val; flatness = val; });
				crestBus.get({ arg val; crest = val; });
				fluxBus.get({ arg val; flux = val; });
				powerBus.get({ arg val; power = val; });
				slopeBus.get({ arg val; slope = val; });
				xingBus.get({ arg val; zeroXing = val; });
				mfccBus.getn(13, { arg val; mfcc = val; });
				// write to the arrays.
				// This becomes the JSON object cURL'd to the CG server
				dataArray = dataArray ++ "{\\\"rms\\\":" ++ this.rms
							++ ",\\\"speccent\\\":" ++ this.centroid
							++ ",\\\"flatness\\\":" ++ this.flatness
							++ ",\\\"speccrest\\\":" ++ this.crest
							++ ",\\\"specflux\\\":" ++ this.flux
							++ ",\\\"specpower\\\":" ++ this.power
							++ ",\\\"specslope\\\":" ++ this.slope
							++ ",\\\"zeroxing\\\":" ++ this.zeroXing
							++ ",\\\"mfcc1\\\":" ++ this.mfcc[0]
							++ ",\\\"mfcc2\\\":" ++ this.mfcc[1]
							++ ",\\\"mfcc3\\\":" ++ this.mfcc[2]
							++ ",\\\"mfcc4\\\":" ++ this.mfcc[3]
							++ ",\\\"mfcc5\\\":" ++ this.mfcc[4]
							++ ",\\\"mfcc6\\\":" ++ this.mfcc[5]
							++ ",\\\"mfcc7\\\":" ++ this.mfcc[6]
							++ ",\\\"mfcc8\\\":" ++ this.mfcc[7]
							++ ",\\\"mfcc9\\\":" ++ this.mfcc[8]
							++ ",\\\"mfcc10\\\":" ++ this.mfcc[9]
							++ ",\\\"mfcc11\\\":" ++ this.mfcc[10]
							++ ",\\\"mfcc12\\\":" ++ this.mfcc[11]
							++ ",\\\"mfcc13\\\":" ++ this.mfcc[12]
							++ "}";
				// update total number of readings
				numOfReadings = numOfReadings + 1;
				// optional prints
				if( verbose > 3, {
					"Cycle Reading Number: ".post;
					numOfReadings.postln;
					(" RMS: "++this.rms).postln;
					(" Zero Corssing Freq: "++this.zeroXing).postln
					(" Spectral Centroid: "++this.centroid).postln;
					(" Spectral Crest: "++this.crest).postln;
					(" Spectral Flux: "++this.flux).postln;
					(" Spectral Power: "++this.power).postln;
					(" Spectral Slope: "++this.slope).postln;
					(" Spectral Flatness: "++this.flatness).postln;
					(" MFCC Array: "++this.mfcc).postln;
					"".postln;
				});
				// have we reached the number of features to be sent?
				if( numOfReadings < numOfFeatures,
					// if the max has not been reached then add a new field to the arrays
					{	dataArray = dataArray ++ ","; },
					// Else send the info out and reset vars
					{
						// create the cURL query
						qry="curl -X POST -H \"Content-Type: application/json\" -d ";
						qry=qry++"\"{\\\"uid\\\":\\\"" ++ this.uID
							++ "\\\",\\\"time\\\":\\\""
						++ (timeStamp.rawSeconds.round(0.001)-timeOffset)
							++ "\\\",\\\"interval\\\":"++featurePollDelay
							++ ",\\\"count\\\":" ++numOfFeatures
							++ ",\\\"data\\\":["++dataArray ++ "]}\" ";
						qry=qry++"http://citygram.smusic.nyu.edu/postdata/poster.php";
						if(verbose>2,{qry.postln;});
						// bench this to know timing tendencies
						{
							// open a unix terminal pipe connection and send out the query

							pipe_ = Pipe.new(qry, "r");

							// update the total number of readings which have been sent
							if( verbose > 0,{
								"Time Stamp: ".post; timeStamp.post; " -- EPOCH: ".post;
								(timeStamp.rawSeconds.round(0.001)-timeOffset).postln;
								line_ = pipe_.getLine;
								while({line_.notNil}, { line_.postln; line_ = pipe_.getLine; });
								"Total Number of Readings: ".post; this.totalReadings.postln;
								if( verboseBench.not, {"".postln;});
							});
							if( verboseBench, { "SENDING TO THE SERVER TOOK ".post; });
							// close the pipe to avoid that nasty buildup
							pipe_.close;
							totalReadings = this.totalReadings + numOfReadings;
						}.bench(verboseBench);
						if( verboseBench, { "".postln; }; );
						// resets
						numOfReadings = 0; dataArray = "";
				});
				// the amount of time to wait between each sample
				this.featurePollDelay.wait;
			});
			},
			SystemClock
		);
	}


	/***************************************************************************************/
	// The following two methods are used to control the streaming and analysis process
	/***************************************************************************************/

	// STREAM FEATURES TO CITYGRAM SERVER FROM AN AUDIO HARDWARE MIC BUS
	streamFromMic {
		arg in = 0, featurePollDelay, pushDelay;
		var serverOn;
		// if the user supplied featurePollDelay or pushDelay update these
		if(featurePollDelay.notNil, {this.featurePollDelay=featurePollDelay;});
		if(pushDelay.notNil, {this.pushDelay=pushDelay;});
		// check that server is on
		serverOn = 	Server.default.serverRunning;
		if( serverOn.not, { "".postln;"BOOT YOUR SERVER AND TRY AGAIN".postln; ^nil; });
		// Start the extraction synth and set the audio buses to use
		extractionSynth = Synth(\citygramExtractor_SoundIn, [bus: in, rmsOut: rmsBus, centOut: centBus,
			flatOut: flatBus, crestOut: crestBus, fluxOut: fluxBus, powerOut: powerBus,
			slopeOut: slopeBus, xingOut: xingBus, mfccOut: mfccBus] );
		// start the streaming task
		cgSendTask.start;
	}

	// STREAM FEATURES TO CITYGRAM SERVER from an Audio Bus
	streamFromBus {
		arg in = 0, featurePollDelay, pushDelay, startTime;
		var serverOn;
		// if the user supplied featurePollDelay or pushDelay update these
		if(featurePollDelay.notNil, {this.featurePollDelay=featurePollDelay;});
		if(pushDelay.notNil, {this.pushDelay=pushDelay;});
		if( startTime.notNil,
			{
				if( this.setStartTime(startTime),
					{},
					{"startTime not valid".postln; ^nil}
				)
			}
		);
		// check that server is on
		serverOn = 	Server.default.serverRunning;
		if( serverOn.not, { "".postln;"BOOT YOUR SERVER AND TRY AGAIN".postln; ^nil; });
		extractionSynth = Synth.tail( defName: \citygramExtractor_Bus, args: [bus: in, rmsOut: rmsBus, centOut: centBus,
			flatOut: flatBus, crestOut: crestBus, fluxOut: fluxBus, powerOut: powerBus,
			slopeOut: slopeBus, xingOut: xingBus, mfccOut: mfccBus] );
		cgSendTask.start;
	}

	// check and set the starttime for bus streaming
	// This would be used if you pre-recorded an audio sample
	// and wanted to run it in after the fact
	// Need to check the database though to make sure the timestamp
	// is later than the last timestamp
	setStartTime {
		arg start_timestamp;

		if( (start_timestamp > this.lastTimestamp),
			{ startTime = start_timestamp; ^true},
			{
				"ERROR: desired timestamp is invalid".postln;
				"Must be greater than RSD's last timestamp - ".post;
				this.lastTimestamp.postln;
				^false
			}
		);

	}

	startTime {
		^startTime
	}

	// Clean up the server. This tells the server that this RSD is done streaming for the moment
	closeStream {
		var qry, pipe_, line_;
		// set rsd to inActive on the Server
		qry="curl -X POST -H \"Content-Type: application/json\" -d ";
		qry=qry++"\"{\\\"uid\\\":\\\""++this.uID++ "\\\"}\" ";
		qry=qry++"http:\/\/citygram.smusic.nyu.edu/postdata/poster.php";
		pipe_ = Pipe.new(qry, "r"); line_ = pipe_.getLine;
		while({line_.notNil}, {line_.postln; line_ = pipe_.getLine; });
		// close the pipe to avoid that nasty buildup
		pipe_.close;
		if( verbose > 0, {
			"Stopped sending feature data to the database.".postln;
			("Sent " ++ this.totalReadings ++ " feature sets.").postln;
		});
	}

	// Gracefully stop all of the processes
	stop {
		cgSendTask.stop;
		extractionSynth.free;
		this.closeStream;
		^this.totalReadings
	}

	// free all busses and such
	free {
		rmsBus.free; centBus.free; flatBus.free; crestBus.free; fluxBus.free;
		powerBus.free; slopeBus.free; xingBus.free; mfccBus.free;
		username=nil; password=nil; uID=nil;
	}

}