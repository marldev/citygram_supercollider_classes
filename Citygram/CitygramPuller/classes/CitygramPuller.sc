/* *************************************************************************
 * Citygram > CitygramPuller
 * This is the SuperCollider Class file for Citygram Pulling
 *
 * The Citygram project can be found at http://citygram.smusic.nyu.edu
 * Tae-Hong Park is the Principal Investigator
 *
 * This SuperCollider Library was written by the Citygram team and led
 * by Michael Musick.  Questions can be directed to him at musick@nyu.edu .
 *
 * Disclaimer: We are still in the earlier stages of this project, and
 * 				as such, things are likely to break.  Sorry. We will
 * 				work as quick as is possible to fix any problems that arise.
 *
 * Version 1.0 - 04.23.2015
 **************************************************************************/

CitygramPuller {
	var <uID, <historicTime, <loopTime, username, password, <feature, <>verbose;
	var <>frameRate=0.1, <endTime, <currentTime, <startTime, <timestamp, <pullTime;
	var <>windowSize=60, <listSize=1, featureToPull, type;
	var <rmsBus, <centroidBus, <flatnessBus, <crestBus, <fluxBus, <powerBus;
	var <slopeBus, <xingBus, <mfccBus, <featureBusArray;
	var <rmsVal, <centroidVal, <flatnessVal, <crestVal, <fluxVal, <powerVal;
	var <slopeVal, <xingVal, <mfccVal, <featureValArray;
	var lat, lng, isActive;
	var task, <waitingData, dataReady, pullNow = false;
	var oscSend, <sendAsOSC, <oscPort;

	// instantiate a new object
	*new {
		arg uID, historicTime, loopTime, username, password, set_rmsBus, set_centroidBus,
			set_flatnessBus, set_crestBus, set_fluxBus,
			set_powerBus, set_slopeBus, set_xingBus, set_mfccBus;
		^super.newCopyArgs( uID, historicTime, loopTime );
	}

	// pull all available features from one RSD
	*oneRSD {
		arg uID, historicTime, loopTime, username, password, verbose=0, sendAsOSC=false, oscPort=7375, set_rmsBus=true,
			set_centroidBus=true, set_flatnessBus=true, set_crestBus=true, set_fluxBus=true,
			set_powerBus=true, set_slopeBus=true, set_xingBus=true, set_mfccBus=true;
		var inst, exists;
		// ensure that the uID was supplied
		if(uID.isNil, {"ERROR: Please supply a uID!".postln; ^nil});
		if(username.isNil, {username="not set"; password="not set";});
		// if a historicTime was supplied then copy it over
		if(historicTime.isNil, {historicTime=nil; loopTime=nil;});
		// TODO: check to make sure loopTime > historicTime ifset
		// instantiate
		inst = this.new(uID,historicTime,loopTime,username,password);
		// check that the RSD exists on the Citygram Server
		exists=inst.checkAllRSDsAreValid([uID].asArray, verbose);
		if(exists!=true, {^exists});
		// copy the verbose var over
		inst.verbose=verbose;
		// copy over OSC Send
		inst.oscInit(sendAsOSC, oscPort);
		// inst.sendAsOSC=true;
		// if(sendAsOSC, {inst.oscPort=oscPort});
		// post information
		if(verbose>0, {("A Citygram Puller, with UID "++inst.uID).postln;});
		// allocate buses
		inst.allocBuses( set_rmsBus, set_centroidBus, set_flatnessBus, set_crestBus, set_fluxBus,
						set_powerBus, set_slopeBus, set_xingBus, set_mfccBus);
		// instantiate the pulling task
		inst.storeTask1;
		^inst; // return the instance
	}

	// pull one specified feature from a list of RSDs
	*multipleRSDs {
		arg uID, historicTime, loopTime, username, password, feature=\rms, verbose=0, sendAsOSC=false, oscPort=7375;
		var inst, exists;
		if(uID.isNil, {"ERROR: Please supply a uID!".postln; ^nil});
		if(uID.isString, {uID=[uID]; });
		if(username.isNil, {username="not set"; password="not set";});
		if(historicTime.isNil, {historicTime=nil; loopTime=nil;});
		inst = this.new(uID,historicTime,loopTime,username,password,feature);
		inst.verbose=verbose;
		// copy over OSC Send
		inst.oscInit(sendAsOSC, oscPort);
		// set the list size and make sure that the value was entered into an array, otherwise fix
		inst.setListSize(uID.size);
		exists=inst.checkAllRSDsAreValid(inst.uID, verbose);
		if(exists!=true, {^exists});
		inst.setFeature(feature);
		if(verbose>0, {("A Citygram Puller, with the following "++inst.listSize++" UIDs "++inst.uID).postln;});
		inst.storeTaskMany;
		^inst;
	}

	// check that all rsds are valid in the DB.  Also retrieve lat/lng and isActive flag
	// have not tied lat/lng or isActive to class variables yet
	checkAllRSDsAreValid{
		arg rsds, verbose;
		var qry, pipe_, line_, ret, success=true;
		// the curl call
		qry="curl -X POST -H \"Content-Type: application/json\" -d \"{\\\"rsd\\\":[";
		for( 0, this.listSize-1, { arg i; qry=qry++"\\\""++rsds[i]++"\\\"";
			if( (i<(this.listSize-1)), { qry=qry++","; });
		});
		qry=qry++"]}\" http://citygram.smusic.nyu.edu/streamdata/getLatLngs.php";
		if(verbose>2, {qry.postln;});
		// curl call through terminal pipe
		pipe_=Pipe.new(qry,"r"); line_=pipe_.getLine;
		while({line_.notNil},{ret=ret++line_; line_=pipe_.getLine;});
		pipe_.close;
		// transfrom the returned string in to a data dictionary
		ret=ret.parseYAML["data"];
		// check that the number returned equals the number of RSDs supplied by the user.
		// If not, then their are errors
		success=(ret["size"].asInteger==this.listSize);
		if(verbose>3, {ret.postln;});
		if(success==false, {
			var returnedRSDs=Array.newClear(ret["size"].asInteger);
			var wrongRSDs=Array.newClear(this.listSize-ret["size"].asInteger);
			var arrPos=0;
			"Your rsd list contains errors, the following RSDs do not exist!".postln;
			// find the incorrect RSDs
			(ret["size"].asInteger).do({arg i; returnedRSDs[i]=ret[i.asString]["rsd"];});
			rsds.do({
				arg rsdName, i;
				returnedRSDs.do({ arg rsdRet, k; if(rsdName==rsdRet, {rsds[i]=nil;}); });
				if(rsds[i].notNil, {wrongRSDs[arrPos]=rsdName; arrPos=arrPos+1;});
			});
			^wrongRSDs.postln;
		});
		^success
	}

	// set the list size (number of RSDs for .multipleRSDs() )
	setListSize {
		arg size=1;
		listSize=size;
	}

	// set the feature to pull for .multipleRSDs()
	setFeature {
		arg featureToSet;
		// set the class variable
		feature=featureToSet;
		// switch statement
		featureToPull = case
		{ featureToSet==\rms }  { "td_rms" }
		{ featureToSet==\centroid }  { "fd_centroid" }
		{ featureToSet==\flatness }  { "fd_specFlatness" }
		{ featureToSet==\crest }  { "fd_specCrest" }
		{ featureToSet==\flux }  { "fd_specFlux" }
		{ featureToSet==\power }   { "fd_specPower" }
		{ featureToSet==\slope }  { "fd_specSlope" }
		{ featureToSet==\xing }  { "td_zeroXingFreq" }
		{ featureToSet==\mfcc }  { "mfcc" };

		// instantiate the feature value array and timestamp array
		featureValArray = Array.newClear( this.listSize );
		timestamp = Array.newClear( this.listSize );
		// check that the buses have not been set, if they have clear them out first
		if(featureBusArray.notNil, {featureBusArray.do({ arg bus, i; featureBusArray[i].free; }); });
		// set the feature bus array
		featureBusArray = Array.fill( this.listSize, {
			if( featureToSet==\mfcc, {Bus.control(numChannels:13)}, {Bus.control;});
		});
	}

	// allocated buses for feature values for .oneRSD()
	allocBuses{
		arg set_rmsBus, set_centroidBus, set_flatnessBus, set_crestBus, set_fluxBus,
			set_powerBus, set_slopeBus, set_xingBus, set_mfccBus;
		if(set_rmsBus.and(rmsBus.isNil), {rmsBus=Bus.control;});
		if(set_centroidBus.and(centroidBus.isNil), {centroidBus=Bus.control;});
		if(set_flatnessBus.and(flatnessBus.isNil), {flatnessBus=Bus.control;});
		if(set_crestBus.and(crestBus.isNil), {crestBus=Bus.control;});
		if(set_fluxBus.and(fluxBus.isNil), {fluxBus=Bus.control;});
		if(set_powerBus.and(powerBus.isNil), {powerBus=Bus.control;});
		if(set_slopeBus.and(slopeBus.isNil), {slopeBus=Bus.control;});
		if(set_xingBus.and(xingBus.isNil), {xingBus=Bus.control;});
		if(set_mfccBus.and(mfccBus.isNil), {mfccBus=Bus.control(numChannels:13);});
	}

	// initialize OSC send
	oscInit{
		arg sendAsOSC_, oscPort_;
		sendAsOSC = sendAsOSC_;
		if( sendAsOSC, {
			oscPort = oscPort_;
			oscSend = NetAddr.new( "127.0.0.1", oscPort_);
			"OSC Sending is enabled".postln;
		});
	}

	// CHANGE RSD UID AND CHECK IF VALID
	changeAndCheckRSD{
		arg newUID = "", start=nil, end=nil;
		var success = false, uID_;

		if( uID.size>0, {
			if( type==1, {uID_ = [newUID].asArray;});
			success = this.checkAllRSDsAreValid(uID_, this.verbose);
		});
		if( success.not, {"RSD UID WAS NOT VALID. NOT CHANGED!".postln;});
		if( success, {uID = newUID; if(verbose>0, {"RSD change successful".postln;});});
		this.changeTime(start,end);
		^success
	}

	// CHANGE RSD UID AND DO NOT CHECK
	// This can be dangerous
	setUID{
		arg newUID = "", start=nil, end=nil;
		uID = newUID;
		this.changeTime(start,end);
		^uID
	}

	// CHANGE PLAYBACK TIME
	changeTime{
		arg start=nil, end=nil;
		if(start.notNil, {historicTime = start;});
		if(end==0, {loopTime=nil;});
		if(end.notNil, {loopTime=end;});

		pullTime = nil;
		pullNow = true;
		^historicTime
	}

	/***************************************************************************************/
	// The following two methods apply to the .oneRSD() process
	/***************************************************************************************/

	// this is the "get data" function for .oneRSD()
	// NOTE: although this is technically an asynchronous method,
	// PIPE will cause all other processes to pause while executing
	// TODO: create a a UGen for cURL calls??
	getDataForOneRSD{
		var qry, ret="";
		var pipe_, line_;

		// create curl call to get data
		qry="curl -X POST -d \"rsd="++this.uID;
		if(historicTime.notNil.and(pullTime.isNil), { pullTime = this.historicTime; });
		if(historicTime.notNil, {
			qry=qry++"&historicTime="++this.pullTime;
		});
		if(windowSize.notNil, {qry=qry++"&windowSize="++this.windowSize});
		qry=qry++"\" http://citygram.smusic.nyu.edu/streamdata/pull_oneRSD.php";
		// get the data through a pipe to terminal
		if(verbose>2, {qry.postln;});
		pipe_=Pipe.new(qry,"r"); line_=pipe_.getLine;
		while({line_.notNil},{ret=ret++line_; line_=pipe_.getLine;});
		pipe_.close;
		// put the string into a dictionary representation
		waitingData=ret.parseYAML["data"];
		// let the task know the data is ready
		^dataReady=true
	}

	// store task for .oneRSD()
	storeTask1{
		// initiate the type;
		type = 1;
		// instantiate and clear out the mfcc feature value array
		mfccVal = Array.newClear(13);
		// create a netaddress to send osc messages on
		// sendAsOSC.postln;
		// if(sendAsOSC, {this.sendOsc = NetAddr.new( "127.0.0.1", oscPort)});
		// the main task
		task=Task({
			var currentFeature, featureListSize=0, frameEnd, data, rate, valid;
			var requestTime, continueLoop, tempWait, outLoop1, outLoop2;
			// set an initial time in order to track how long this takes
			outLoop1=outLoop2=Date.getDate.rawSeconds;
			// get the first set of data
			dataReady=false;
			inf.do({
				this.getDataForOneRSD;
				if(this.verbose>2, {
					outLoop2=Date.getDate.rawSeconds;
					("Loop took: "++(outLoop2-outLoop1)++" secs").postln;
					outLoop1=outLoop2;
				});
				// track the time this takes to try and adjust the wait time in the loop
				requestTime = {
					// update the frame rate once every block of features
					rate = this.frameRate;
					// pause till the data is ready
					if(dataReady.not, {
						if(this.verbose>1, {"Have to wait for data..".post});
						while({dataReady.not}, {0.01.wait;});
						if(this.verbose>1, {"Got the data".postln;});
					});
					// double check that the data is not nil
					if(waitingData.notNil, {
						// copy into the main var
						data=waitingData;
						// set variables for this data set
						// this is the end time for this feature set
						endTime=(data["endTime"]).asFloat;
						// this is the start time for this feature set
						startTime=(data["startTime"]).asFloat;
						// starttime also become the first frames start time
						currentTime=this.startTime;
						// set the first frames end time
						frameEnd=this.currentTime+rate;
						// reset the current feature count
						currentFeature=0;
						featureListSize=(data["featSize"]).asInt;
						// if historic time is being used, then update the variable
						if(pullTime.notNil,
							{pullTime=data["endTime"];
						});
						continueLoop=true;
						if(this.verbose>2, {
							("start time: "++this.startTime).postln;
							("current time: "++this.currentTime).postln;
							("end time: " ++ this.endTime).postln;
							("frame end: "++frameEnd).postln;
							("pull time: "++this.pullTime).postln;
							("list size: "++featureListSize).postln;
							("currentfeature: "++currentFeature).postln;
							("continueLoop: "++continueLoop).postln;
						});

					}, {continueLoop=false;});

					// get more data for the next loop
					dataReady=false;
					if(this.verbose>2, {"Server request took ".post;});
				}.bench((this.verbose>2));
				// was all successful? if not wait two seconds and try again
				// this happens when an RSD is not streaming for the specified time period
				continueLoop=continueLoop.and((currentFeature<featureListSize).and(this.currentTime<this.endTime));
				if(continueLoop,
					{
						tempWait=rate-requestTime;
						if(tempWait<0, {tempWait=0});
						tempWait.wait;
					},{
						if(this.verbose>1, {
							"No feature data, for window: ".post;
							this.currentTime.post; " .. ".post; this.endTime.postln;
							"checking again in 2 seconds.".postln;
						});
						if(pullTime.notNil,
							{pullTime=data["endTime"];
						});
						2.wait;
					}
				);

				// the main loop.  This is run at "frameRate" and controls the flow of the features returned
				// currentFeature = the feature number in the returned data set
				({continueLoop}).while({
					// update wait time
					rate = this.frameRate;
					// get the current feature timestamp
					timestamp=data["featurelist"][currentFeature]["timeStamp"].asFloat;
					if(this.verbose>3, {
						("current time "++this.currentTime++". current feature timestamp "++this.timestamp).postln}
					);
					// make sure the timestamoe falls under the frame end time.  Else wait till the next frame to update
					if(this.timestamp<frameEnd, {valid=true;},{valid=false;});
					// if the current features timestamp is less than the frames start time
					// then loop to the next valid feature (based on timestamps)
					if( valid.and(this.timestamp<this.currentTime), {
						({valid.and(this.timestamp<this.currentTime)}).while({
							currentFeature=currentFeature+1;
							if(currentFeature<featureListSize,
								{timestamp=data["featurelist"][currentFeature]["timeStamp"].asFloat;},
								// if this process overuns the number of features then we need more data!
								{valid=false;}
							);
						});
					});
					// if the current feature num is succesful found to be in this frames window
					// then update values
					if( valid, {
						rmsVal=data["featurelist"][currentFeature]["rms"].asFloat;
						centroidVal=data["featurelist"][currentFeature]["specCentroid"].asFloat;
						flatnessVal=data["featurelist"][currentFeature]["specFlatness"].asFloat;
						crestVal=data["featurelist"][currentFeature]["specCrest"].asFloat;
						fluxVal=data["featurelist"][currentFeature]["specFlux"].asFloat;
						powerVal=data["featurelist"][currentFeature]["specPower"].asFloat;
						slopeVal=data["featurelist"][currentFeature]["specSlope"].asFloat;
						xingVal=data["featurelist"][currentFeature]["zeroXing"].asFloat;
						mfccVal=data["featurelist"][currentFeature]["mfcc"].asFloat;
						// set any buses that are active
						if(rmsBus.notNil, {rmsBus.value=rmsVal;});
						if(centroidBus.notNil, {centroidBus.value=centroidVal;});
						if(flatnessBus.notNil, {flatnessBus.value=flatnessVal;});
						if(crestBus.notNil, {crestBus.value=crestVal;});
						if(fluxBus.notNil, {fluxBus.value=fluxVal;});
						if(powerBus.notNil, {powerBus.value=powerVal;});
						if(slopeBus.notNil, {slopeBus.value=slopeVal;});
						if(xingBus.notNil, {xingBus.value=xingVal;});
						if(mfccBus.notNil, {mfccBus.setn(mfccVal);});

						if( sendAsOSC, {
							oscSend.sendMsg("/fromSCCG/rms", rmsVal);
							oscSend.sendMsg("/fromSCCG/centroid", centroidVal);
							oscSend.sendMsg("/fromSCCG/flatness", flatnessVal);
							oscSend.sendMsg("/fromSCCG/crest", crestVal);
							oscSend.sendMsg("/fromSCCG/flux", fluxVal);
							oscSend.sendMsg("/fromSCCG/power", powerVal);
							oscSend.sendMsg("/fromSCCG/slope", slopeVal);
							oscSend.sendMsg("/fromSCCG/xing", xingVal);
							oscSend.sendMsg("/fromSCCG/mfcc1", mfccVal[0]);
							oscSend.sendMsg("/fromSCCG/mfcc2", mfccVal[1]);
							oscSend.sendMsg("/fromSCCG/mfcc3", mfccVal[2]);
							oscSend.sendMsg("/fromSCCG/mfcc4", mfccVal[3]);
							oscSend.sendMsg("/fromSCCG/mfcc5", mfccVal[4]);
							oscSend.sendMsg("/fromSCCG/mfcc6", mfccVal[5]);
							oscSend.sendMsg("/fromSCCG/mfcc7", mfccVal[6]);
							oscSend.sendMsg("/fromSCCG/mfcc8", mfccVal[7]);
							oscSend.sendMsg("/fromSCCG/mfcc9", mfccVal[8]);
							oscSend.sendMsg("/fromSCCG/mfcc10", mfccVal[9]);
							oscSend.sendMsg("/fromSCCG/mfcc11", mfccVal[10]);
							oscSend.sendMsg("/fromSCCG/mfcc12", mfccVal[11]);
							oscSend.sendMsg("/fromSCCG/mfcc13", mfccVal[12]);
						});

						// update the feature count
						currentFeature=currentFeature+1;
					});

					// update the times for the next frame
					currentTime=frameEnd;
					frameEnd=this.currentTime+rate;
					// Continue working through this data set?
					continueLoop=((currentFeature<featureListSize).and(this.currentTime<this.endTime));
					// check if loop back time was reached
					if(historicTime.notNil.and(loopTime.notNil), {if(frameEnd>loopTime, {this.changeTime();}); });
					// check if pull now?
					if( pullNow, {continueLoop=false; pullNow=false;});
					// if we are done with this data set, then it is time to exit the loop and get more data
					// otherwise, wait the specified frameRate
					if(continueLoop, {rate.wait;});
				});
			});
			}, SystemClock
		);
	}


	/***************************************************************************************/
	// The following two methods apply to the .multipleRSDs() process
	/***************************************************************************************/

	getDataForMultiRSDs{
		var qry, ret="";
		var pipe_, line_;
		// construct a curl call for the requested RSDs
		qry="curl -X POST -H \"Content-Type: application/json\" -d ";
		qry=qry++"\"{\\\"rsd\\\":[";
		for( 0, this.listSize-1, { arg i; qry=qry++"\\\""++this.uID[i]++"\\\"";
			if( (i<(this.listSize-1)), { qry=qry++","; });
		});
		qry=qry++"],";
		if(historicTime.notNil.and(pullTime.isNil), { pullTime = this.historicTime; });
		if(historicTime.notNil, {
			qry=qry++"\\\"historicTime\\\":"++this.pullTime++",";
		});
		if(windowSize.notNil, {qry=qry++"\\\"windowSize\\\":"++this.windowSize++","});
		qry=qry++"\\\"feature\\\":\\\""++featureToPull++"\\\"}\"";
		qry=qry++" http://citygram.smusic.nyu.edu/streamdata/pull_multiRSDs.php";
		if(verbose>2, {qry.postln;});
		// get the data from PIPE
		pipe_=Pipe.new(qry,"r"); line_=pipe_.getLine;
		while({line_.notNil},{ret=ret++line_; line_=pipe_.getLine;});
		pipe_.close;
		waitingData=ret.parseYAML["data"];
		dataReady=true;
	}

	storeTaskMany{
		// initiate the type;
		type = 2;
		task=Task({
			var currentFeature, featureListSize=0, frameEnd, data, rate, valid;
			var requestTime, continueLoop, tempWait, outLoop1, outLoop2;
			currentFeature = Array.newClear( this.listSize );
			featureListSize = Array.newClear( this.listSize );
			outLoop1=outLoop2=Date.getDate.rawSeconds;
			// get the initial data set
			dataReady=false;
			inf.do({
				this.getDataForMultiRSDs;
				if(this.verbose>2, {
					outLoop2=Date.getDate.rawSeconds;
					("Loop took: "++(outLoop2-outLoop1)++" secs").postln;
					outLoop1=outLoop2;
				});
				requestTime = {
					rate = this.frameRate;
					if(dataReady.not, {
						if(this.verbose>1, {"Have to wait for data..".post});
						while({dataReady.not}, {0.01.wait;});
						if(this.verbose>1, {"Got the data".postln;});
					});
					if(waitingData.notNil, {
						// transfer the data to this methods personal var
						data=waitingData;
						// set the appropriate timestamps for this data set
						endTime=(data["endTime"]).asFloat;
						startTime=(data["startTime"]).asFloat;
						currentTime=this.startTime;
						frameEnd=this.currentTime+rate;
						// reset the current feature counter for each RSD
						// RSDs have independent feature counters because they may have streamed at different rates
						// so it may be neccessary to work through the data at different rates
						currentFeature.do({ arg item, i; currentFeature[i]=0;});
						// get the number of samples for each RSD
						featureListSize.do({ arg item, i; featureListSize[i]=data[i.asString]["featSize"].asInt;});
						// if historic time is being used, then update the variable
						if(pullTime.notNil,
							{pullTime=data["endTime"];
						});
						continueLoop=true;
					}, {continueLoop=false;});
					// get more data for next time
					dataReady=false;
					if(this.verbose>1, {"Server request took ".post;});
				}.bench((this.verbose>1));

				continueLoop=continueLoop.and(this.currentTime<this.endTime);
				if(continueLoop,
					{
						tempWait=rate-requestTime;
						if(tempWait<0, {tempWait=0});
						tempWait.wait;
					},{
						if(this.verbose>1, {
							"No feature data, for window: ".post;
							this.currentTime.post; " .. ".post; this.endTime.postln;
							"checking again in 2 seconds.".postln;
						});
						2.wait;
					}
				);

				// main loop to process the feature set
				({continueLoop}).while({
					// update wait time
					rate = this.frameRate;
					// get the timestamp for the each RSDs current feature number
					timestamp.do({ arg item, i;
						// are there more features to read?
						valid=(currentFeature[i]<featureListSize[i]);
						if(valid, {timestamp[i]=data[i.asString]["featurelist"][currentFeature[i]]["timeStamp"].asFloat;});
						if(this.verbose>3, {
							("current time "++this.currentTime++". current feature timestamp "++this.timestamp[i]).postln;}
						);
						// are the timestamps within the frame timestamps
						valid=(valid.and(this.timestamp[i]<frameEnd));
						// if the RSDs currentfeature timestamp is prior to the frame start,
						// then advance to the next valid feature number, as according to timestamp
						if( valid.and(this.timestamp[i]<this.currentTime), {
							({valid.and(this.timestamp[i]<this.currentTime)}).while({
								currentFeature[i]=currentFeature[i]+1;
								if(currentFeature[i]<featureListSize[i],
									{timestamp[i]=data[i.asString]["featurelist"][currentFeature[i]]["timeStamp"].asFloat;},
									// overran the feature list size
									{valid=false;}
								);
							});
						});
						// if the timestamp is within the frame window then copy the values over
						if( valid, {
							featureValArray[i]=data[i.asString]["featurelist"][currentFeature[i]]["val"].asFloat;
							if(this.verbose>3, {(""++this.feature++" for "++this.uID[i]++": "++featureValArray[i]).postln;});
							// if using a bus then set it
							if(featureBusArray[i].notNil, {
								// check if it is a single value bus or 13 value mfcc bus
								if(this.feature==\mfcc, {featureBusArray[i].setn(featureValArray[i]);}, {featureBusArray[i].set(featureValArray[i]);});
								// Set OSC Send MSGs if appropriate
								if( sendAsOSC, {
									var adr = "/fromSCCG/feature_";
									adr = adr++i;
									oscSend.sendMsg(adr, featureValArray[i]);
								});

							});
							// advance the current feature
							currentFeature[i]=currentFeature[i]+1;
						});
					});

					// adjust the next frames times
					currentTime=frameEnd;
					frameEnd=this.currentTime+rate;
					// are we still within the feature set time window
					continueLoop=(this.currentTime<this.endTime);
					// check if loop back time was reached
					if(historicTime.notNil.and(loopTime.notNil), {if(this.currentTime>loopTime, {this.changeTime();}); });
					// check if pull now?
					if( pullNow, {continueLoop=false; pullNow=false;});
					// if so wait the frameRate time, otherwise get more data!
					if(continueLoop, {rate.wait;});
				});
			});
			}, SystemClock
		);
	}


	/***************************************************************************************/
	// These are the start and stop methods for both types of pulling
	/***************************************************************************************/

	getData{
		arg historicTime, windowSize, feature;
		{
			if(windowSize.notNil, {this.windowSize=windowSize;});
			if(historicTime.notNil, {this.historicTime=historicTime;});
			if(feature.notNil, {this.setFeature(feature);});
			if(type==1, {this.getDataForOneRSD;});
			if(type==2, {this.getDataForMultiRSDs;});
			endTime=(this.waitingData["endTime"]).asFloat;
			startTime=(this.waitingData["startTime"]).asFloat;
		}.bench(verbose>0);
		^this.waitingData;
	}

	// start the task
	start{
		// this method allows the user to enter a histricTime to use
		arg start, end, windowSize;
				if(start.notNil, {historicTime = start;});
		if(end==0, {loopTime=nil;});
		if(end.notNil, {loopTime=end;});
		if(windowSize.notNil, {this.windowSize=windowSize;});
		task.start;
	}

	// stop the task
	stop{
		task.stop;
	}

	// free any buses which were set
	free{
		task.stop;
		rmsBus.free; centroidBus.free; flatnessBus.free; crestBus.free;
		fluxBus.free; powerBus.free; slopeBus.free; xingBus.free; mfccBus.free;
		featureBusArray.do({arg item, i; featureBusArray[i].free;});
		oscSend.disconnect;
		task=nil;
	}


}