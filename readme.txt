SuperCollider Citygram Class Files

PLEASE NOTE: 
These SC Class Files and the Citygram Data API are no longer maintained in this current form and are considered DEPRECATED. 
Instead, this repository exists as documentation of the first API for Citygram. 
To participate or utilize the current Citygram API, please visit the Citygram site (https://citygramsound.com or https://citygramsound.com:4347/).



--------------------------------------------------------------------------
ver. 1.0 4.20.2015
By Michael Musick and the Citygram Team

For more information on the project please visit citygram.smusic.nyu.edu
Or email the Principal Investigator, Tae-Hong Park at Tae.Hong.Park@nyu.edu
Or Michael at musick@nyu.edu

For more information on Michael visit his page at michaelmusick.com

--------------------------------------------------------------------------
COMPATIBILITY:
These classes have only been tested on Mac OS X 10.8-10.10 and SC 3.6.6 and SC 3.7.alpha.
They rely on Unix calls and as such, will likely not work on Linux or Windows based systems. However If you do test it on one of these systems and find it to work, would you please let us know.

--------------------------------------------------------------------------
INSTALLATION:
To install, place the Citygram parent directory where you would typically place
external class files for your SuperCollider build.
This is typically in
	1. /Library/Application Support/SuperCollider/Extensions/
 	   or
	2. /Users/"YourUsername"/Library/Application Support/SuperCollider/Extensions

--------------------------------------------------------------------------
DEPENDENCIES:
The CitygramStreamer class requires the many of the analysis UGens found in the
sc3-plugins package.
	This link can be found on the main downloads page for SuperCollider
	- http://supercollider.sourceforge.net/downloads/
	Or, downloaded directly from
	- http://sourceforge.net/projects/sc3-plugins/files/OSX_3.6/SC3ExtPlugins-universal.dmg/download

--------------------------------------------------------------------------
WHATS INCLUDED:
1. CitygramStreamer
	- This provides the user with the ability to stream features to the
		Citygram database
		( note: prior registration is required on the Citygram website )
2. CitygramPuller
	- This provides the user with the ability to pull current or historical features
		from any Remote Sensing Device (RSD) in the Citygram database.
	- This class allows for the pulling of all available features from a single RSD
		or, the pulling of one feature from one or more RSDs.

--------------------------------------------------------------------------
ABOUT:
Citygram is a large-scale project that began in 2011. Citygram aims to deliver a real-time
visualization/mapping system focusing on non-ocular energies through scale-accurate, non-intrusive, and data-driven interactive digital maps. The first iteration, Citygram One, focuses on exploring spatio-acoustic energies to reveal meaningful information including spatial loudness, traffic patterns, noise pollution, and emotion/mood through audio signal processing and machine learning techniques.

Citygram aims to create a model for visualizing and applying computational techniques to produce metrics and further our knowledge of cities such as NYC and LA. The project will enable a richer representation and understanding of cities defined by humans, visible/invisible energies, buildings, machines, and other biotic/abiotic entities. Our freely Internet-accessible system will yield high impact results that are general enough for a wide range of applications for residents, businesses, and visitors to cities like NYC and LA.

--------------------------------------------------------------------------
Disclaimer: This is a new project, and as such there will certainly be some bugs.
Please let us know any that you find.

Thank you for participating in the Citygram project.